package com.example.helloandroid;

// for Activity and handling Activity.onCreate()
import android.app.Activity; // see https://developer.android.com/reference/android/app/Activity.html
import android.os.Bundle; // see https://developer.android.com/reference/android/os/Bundle.html
// for displaying the "Hello Android" message
import android.widget.TextView; // see https://developer.android.com/reference/android/widget/TextView.html

public class HelloAndroidMainActivity extends Activity {
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // get the message text (see strings.xml)
        String message = getString(R.string.activity_message);
        // get the first? TextView in this context
        TextView textView = new TextView(this);

        // set the text
        textView.setText(message);

        // display the TextView
        this.setContentView(textView);
    }
}
