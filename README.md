# android-helloworld
A simple Android app which only needs a minimal subset of the Android SDK in order for it to build

## Prerequisites

  1. The following Android [build] tools:
    * `adb`
    * `aapt`
    * `dalvik-exchange` (AKA `dx`)
    * `zipalign`
  2. Android SDK Platform
  3. OpenJDK 7 or later

Ubuntu 18.04 installation: `sudo apt install openjdk-8-jdk-headless adb aapt dalvik-exchange zipalign android-sdk-platform-23`

## Handy build targets

  * `apk` -- builds the `.apk` app package
  * `install` -- installs the app on a connected device using `adb`
  * `generatedcode` -- re-generates generated code (useful if you want to look at `R.java`)
  * `clean` -- removes generated code

## Thanks

  * to Armen Avetisyan whose [project](https://github.com/skanti/Android-Manual-Build-Command-Line) I referenced a LOT
  * to Jason Sankey (and Daniel Ostermeier) for documenting the [Android build process](http://www.alittlemadness.com/2010/06/07/understanding-the-android-build-process/)

