# Android build tools
AAPT ?= aapt
DX ?= dalvik-exchange
ZIPALIGN ?= zipalign
# Android platform tools
ADB ?= adb
# Android SDK Platform JAR
PLATFORMJAR ?= /usr/lib/android-sdk/platforms/android-23/android.jar

# Java tools
JAVAC ?= javac
KEYTOOL ?= keytool
JARSIGNER ?= jarsigner

# APK signing parameters
KEYSTORE_ALIAS = "KeystoreAlias"
KEYSTORE_PASSPHRASE = "012345"

# finds all the .java files so we can invoke the compiler only once because its initialization is slow
JAVASRC = $(shell find src -type f -iname "*.java")



# default target that points to the target for the final APK
.PHONY: all
all: apk

# re-generate generated code
.PHONY: generatedcode
generatedcode: src/com/example/helloandroid/R.java

# build the app package
.PHONY: apk
apk: build/apk/aligned.apk

# install app on a connected device
.PHONY: install
install: build/apk/aligned.apk
	$(info INFO: please connect device)
	$(ADB) wait-for-any-device
	$(ADB) install -r build/apk/aligned.apk

# remove all generated files (except for the signing key)
.PHONY: clean
clean:
	rm -f src/com/example/helloandroid/R.java
	rm -rf build



# generate asset code
src/com/example/helloandroid/R.java: meta/AndroidManifest.xml res $(PLATFORMJAR) src
	$(AAPT) package -f -v -M meta/AndroidManifest.xml -S res -I $(PLATFORMJAR) -m -J src

# build the classes.dex file from .class files from .java files
#   NOTE the classes.dex file MUST be named as such or the app will build, but won't run
build/dex/classes.dex: $(PLATFORMJAR) src/com/example/helloandroid/R.java $(JAVASRC)
	mkdir -p build/classes
	$(JAVAC) -source 1.7 -target 1.7 -d build/classes -classpath $(PLATFORMJAR) -sourcepath src $(JAVASRC)
	mkdir -p build/dex
	$(DX) --dex --verbose --output=build/dex/classes.dex build/classes

# build unsigned .apk file
build/apk/unsigned.apk: meta/AndroidManifest.xml res $(PLATFORMJAR) build/dex/classes.dex
	mkdir -p build/apk
	$(AAPT) package -v -f -M meta/AndroidManifest.xml -S res -I $(PLATFORMJAR) -F build/apk/unsigned.apk build/dex

# create a default signing key (see https://developer.android.com/studio/publish/app-signing)
keystore:
	$(warning WARNING: no keystore found at "./keystore", generating an INSECURE one so the build can continue)
	$(KEYTOOL) -genkey -v -keystore keystore -keyalg RSA -keysize 2048 -validity 10000 -alias $(KEYSTORE_ALIAS) -storepass $(KEYSTORE_PASSPHRASE) -dname "CN=commonName, OU=organizationUnit, O=organizationName, L=localityName, S=stateName, C=NET"


# create signed .apk file
build/apk/signed.apk: keystore build/apk/unsigned.apk
	$(JARSIGNER) -verbose -keystore keystore -storepass $(KEYSTORE_PASSPHRASE) -signedjar build/apk/signed.apk build/apk/unsigned.apk $(KEYSTORE_ALIAS)

# align .apk file for performance (technically optional, but highly recommended)
build/apk/aligned.apk: build/apk/signed.apk
	$(ZIPALIGN) -f -v 4 build/apk/signed.apk build/apk/aligned.apk
	$(info INFO: final app APK is located at "./build/apk/aligned.apk")
